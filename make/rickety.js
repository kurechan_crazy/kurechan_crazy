/*
 * Rickety Plugin ver 0.9.5
 * Copyright 2014 Grow Up Solutions
 * Contributing Author: Tomohisa Okada
 */
(function($) {
  $.fn.rickety= function(config) {
    
    //デフォルト設定(json形式)
    var defaults={
      //動作回数
      count   : 20,
      //振れ幅
      swing   : 20,
      //振れ速度
      speed   : 30,
      //再起動するかしないか
      restart : false,
      //モード "normal","up","down"
      mode    : "normal",
      //消えるモード
      hide    : true,
      //モード、"up","down"時の移動距離
      fall    : 3000,
      //回転軸 "none","X","Y","Z","3d"から選択
      rotate  : "none",
      //回転速度（回転軸が設定されている時のみ利用する）
      rotate_speed:5,
      //拡大縮小（デフォルトはオフ）
      scale     : false,
      //最大サイズ（拡大最大サイズ）scaleがtrueの時だけ利用する
      scale_size : 2
    };


   var transform_func = function(element){
     scale_str  = scale_func();
     rotate_str = rotate_func();
     if(scale_str != "" || rotate_str != ""){
       $(element).css("-moz-transform",scale_str + " " + rotate_str);
       $(element).css("-webkit-transform",scale_str + " " + rotate_str);
       $(element).css("-o-transform",scale_str + " " + rotate_str);
       $(element).css("-ms-transform",scale_str + " " + rotate_str);
     }
   };

    /* 
      オブジェクトを回転させる
    */
    var scale_func = function(){
      if(options.scale == false){
        return "";
      }
      scale_cnt = scale_cnt + 0.1;
      scale = (Math.sin(scale_cnt) * options.scale_size) + options.scale_size;
      
      return "scale(" + (scale) + ")";
    }
    
    /* 
      オブジェクトを回転させる
    */
    var rotate_func = function(){
      //回転バリデーション
      if(options.rotate == "none"){
        return "";
      }
      
      //回転角度加算
      rotate_cnt   += Math.floor( Math.random() * options.rotate_speed + 1 );
      
      //回転モードによって、処理を変更する
      if(options.rotate == "X"){
        //X軸を起点に回す
        return "rotateX(" + (rotate_cnt) + "deg)";
      }else if(options.rotate == "Y"){
        //Y軸を起点に回す
        return "rotateY(" + (rotate_cnt) + "deg)";
      }else if(options.rotate == "Z"){
        //Z軸を起点に回す
        return "rotateZ(" + (rotate_cnt) + "deg)";
      }else if(options.rotate == "3d"){
        //3d回転を行う
        return "rotate3d(1,1,1," + (rotate_cnt) + "deg)";
      }
    };
    
    //設定の統合(defaultsの値をconfigで引き渡してきた値で上書き、configで未設定の情報はそのまま維持)
    var options=$.extend(defaults, config);
    
    //起動フラグ（trueになると起動済み）
    var gelflg = false;
    
    //回転角度
    var rotate_cnt   = 0;
    
    //拡大量
    var scale_cnt    = 0;
    
    $(this).click(function(){
      //再起動フラグがfalseかつ、起動フラグがtrueの場合、動かさなくする
      if(!options.restart && gelflg){
        return;
      }
      //回転角度の初期化
      rotate_cnt = 0;
      //起動フラグをon
      gelflg = true;
      
      //親要素をDivタグ化する
      $(this).wrap("<div></div>");
      
      //親要素の設定構築
      $(this).parent().css("position","relative");       //相対位置に変更
      $(this).parent().css("height",$(this).height());   //親要素を対象オブジェクトと同じ高さに変更する
      $(this).parent().css("width",$(this).width());     //親要素を対象オブジェクトと同じ幅に変更する
      
      //親要素の横はAutoにしちゃう（調整の為）
      $(this).parent().css("margin-left","auto");
      $(this).parent().css("margin-right","auto");
      
      //float設定時の対応
      if($(this).css("float")){
        $(this).parent().css("float",$(this).css("float"));
      }
      
      //自分の要素もabsoluteすることによる、変化を行う
      $(this).css("height",$(this).height());            //縦幅を再設定
      $(this).css("width",$(this).width());              //横幅を再設定
      $(this).css("position","absolute");                //絶対値指定
      //$(this).css("z-index","9999");                     //最前面に表示する
      
      //がたがたする
      for(i = 0;i < options.count ;i++){
        
        //振れ幅設定
        left_px = Math.floor( Math.random() * options.swing + 1 );
        top_px  = Math.floor( Math.random() * options.swing + 1 );
        
        //左右指定
        if(Math.floor( Math.random() * 2 ) == 0){
          left_sign = "+=";
        }else{
          left_sign = "-=";
        }
        
        //上下指定
        if(Math.floor( Math.random() * 2 ) == 0){
          top_sign = "+=";
        }else{
          top_sign = "-=";
        }
        
        //Opacity
        if(options.hide == true){
          opacity = (1 / i);
          //モードが"up"と"down"の時は、透明度を0.3加算した状態にしておく
          if(options.mode == "up" || options.mode == "down"){
            opacity += 0.3;
          }
        }else{
          //hideがfalseの場合、Opacityはデフォルトで1.0のまま
          opacity = "1.0";
        }

        //動かす
        $(this).animate({
          "top" : top_sign  + top_px,
          "left": left_sign + left_px,
          "opacity": opacity
        },{duration: options.speed, easing: "swing",
          step: function(s){
            transform_func(this);
            
          }
        });
      }
      
      //最終的な透明度
      if(options.hide == true){
        opacity = "0.0";
      }else{
        opacity = "1.0";
      }
      
      if(options.mode == "up"){
        //上にあがっていくモード
        $(this).animate({
          "top" : "-=" + options.fall,
          "opacity": opacity
        },{duration: options.speed * 50, easing: "swing",
          step: function(s){
            transform_func(this);
          },
          complate:function(){
            //再起動フラグがfalseの時は完全に削除する
            if(!options.restart && options.hide == true){
              $(this).hide();
            }
          }
        });
      }else if(options.mode == "down"){
        //下に落ちていくモード
        $(this).animate({
          "top" : "+=" + options.fall,
          "opacity": opacity
        },{duration: options.speed * 50, easing: "swing",
          step: function(s){
            transform_func(this);
          },
          complate:function(){
            //再起動フラグがfalseの時は完全に削除する
            if(!options.restart && options.hide == true){
              $(this).hide();
            }
          }
        });
      }else{
        //通常モード
        $(this).animate({"opacity": opacity},{duration: options.speed, easing: "swing",
          step: function(s){
            rotate_func(this);
            scale_func(this);
          },
          complate:function(){
            //再起動フラグがfalseの時は完全に削除する
            if(!options.restart && options.hide == true){
              $(this).hide();
            }
          }
        });
      }
    });
  };
})(jQuery);